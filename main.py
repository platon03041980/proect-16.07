#импортируем
from aiogram import Bot, Dispatcher, executor, types
import random
from config import TOKEN
from aiogram.types import ParseMode
from utils import Form
from aiogram.dispatcher import FSMContext
import logging
import aiogram.utils.markdown as md

from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.contrib.middlewares.logging import LoggingMiddleware

bot = Bot(token=TOKEN, )
dp = Dispatcher(bot, storage=MemoryStorage())
dp.middleware.setup(LoggingMiddleware())

logging.basicConfig(level=logging.INFO)

#создание кнопок
@dp.message_handler(commands="start")
async def cmd_start(message: types.Message):
    buttons_text = ("Вывести", "Пополнить", "Перевести деньги на карту", "Посмотреть баланс")
    keyboard_markup = types.ReplyKeyboardMarkup(row_width=4)
    keyboard_markup.row(*(types.KeyboardButton(text) for text in buttons_text))

    await message.reply("Здравствуйте, вы зашли на свой кошелек, что вы хотите сделать?", reply_markup = keyboard_markup)
    await Form.name.set()

#нажатия кнопок
@dp.message_handler(state = Form.name)
async def process_name(message: types.Message, state = FSMContext):
    answer = message.text
    if answer == "Вывести":
        await message.reply("Сколько денег вы хотите вывести?")
        await Form.vivod.set()
    elif answer == "Пополнить":
        await message.reply("Сколько денег вы хотите пополнить?")
        await Form.popoln.set()
    elif answer == ("Перевести деньги на карту"):
        await message.reply("Введите номер карты или номер телефона")
        await Form.perevod.set()
    elif answer == ("Посмотреть баланс"):
        await message.replay
    else:
        await message.reply("Ошибка, попробуйте заново")




@dp.message_handler(state = Form.vivod)
async def process_name(message: types.Message, state = FSMContext):
    answer1 = message.text
    await message.reply("Вы успешно вывели вашу сумму")

@dp.message_handler(state = Form.popoln)
async def process_name(message: types.Message, state = FSMContext):
    answer2 = message.text
    await message.reply("Вы успешно пополнили вашу сумму")

@dp.message_handler(state = Form.perevod)
async def process_name(message: types.Message, state=FSMContext):
    answer3 = message.text
    await message.reply("Сколько рублей перевести?")
    await Form.perevod2.set()

@dp.message_handler(state = Form.perevod2)
async def process_name(message: types.Message, state = FSMContext):
    answer4 = message.text
    await message.reply("Перевод выполнен успешно")
    await Form.perevod3.set()

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
